import React from "react";
export default class Header2 extends React.Component{
    state={
         animal:'I am  a cat'
    }


    showtext=(e)=>{
        let animal= e.target.textContent;

        if(animal=='I am  a cat'){
           this.setState({animal:'I am a dog'})
        }else if(animal=='I am a dog'){
            this.setState({animal:'I am  a cat'})
        }
    }
    render(){
        const {animal}=this.state;
        return(
            <div>
                <h1 onClick={this.showtext}>{animal}</h1>
            </div>
        );
    }
}