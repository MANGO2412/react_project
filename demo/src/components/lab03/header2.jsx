import React from "react";

export default class Header2 extends React.Component{
     render(){
        const {text, mess}=this.props;
        return(
            <h1 onClick={mess}>{text}</h1>
        );
     }
}