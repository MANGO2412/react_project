import React from "react";

export default class Parrafo extends React.Component{
     render(){
        const {text}=this.props;

        return(
            <p>{text}</p>
        );
     }
}