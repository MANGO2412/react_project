import React from "react";

const validate=values=>{
   const errors={}
    if(!values.firstname)
        errors.name="this input is required";
    
   if(!values.lastname)
        errors.lastName="this input is required";

    if(!values.email)
        errors.email="this input is required";

   return errors;
}



export default class Form extends React.Component{
    state={
        errors:{}
    };

    handleChange=({target})=>{
        const {name,value}=target

        console.log(value)
        this.setState({[name]:value})
    }

    handleSubmit=(e)=>{
      e.preventDefault();
      console.log('prevenido')
      const {errors,...sinErrors}=this.state;
      const  results=validate(sinErrors);
      this.setState({errors:results})
      if(!Object.keys(results).length){
         console.log(" send successfully")  
      }
    }

    render(){
        console.log(this.state);
        const {errors}=this.state;
        
        return(
        <form onSubmit={this.handleSubmit}>
            <input type="text" name="firstname" onChange={this.handleChange}/>
              {errors.name && errors.name}
            <input type="text"  name="lastname" onChange={this.handleChange}/>
                {errors.lastName && errors.lastName}
            <input type="email" name="email"    onChange={this.handleChange} />
                {errors.email && errors.email}
            <input type="submit" value="send" />
        </form>
        );
    }
}