import React from "react";

export default  class Header extends React.Component{
    state={
        miau:"I am a miau"
    };

    change(){
        this.setState({miau:"I am a new miau"})
    }

    render(){
        const {miau}=this.state;
        return(
        <header>
            <h1  onClick={this.change}>{miau}</h1>
        </header>
        );
    }
}